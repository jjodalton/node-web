var exec = require("child_process").exec;
var querystring = require("querystring");

function start(response, postData){
	console.log("|__ Request handler 'start' Was called");	
	
	var body = '<html>'+
	'<head>'+
	'<meta http-equiv="Content-Type" content="text/html; '+
	'charset=UTF-8" />'+
	'</head>'+
	'<body>'+
	'<form action="/upload" method="post">'+
	'<textarea name="text" rows="20" cols="60"></textarea>'+
	'<input type="submit" value="Submit text" />'+
	'</form>'+
	'</body>'+
	'</html>';
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write(body);
	response.end();
}

function upload(response, postData){
	console.log("|__ Request handler 'upload' was called");
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.write("Hello! You sent: " + querystring.parse(postData).text);
	response.end();	
}

function test(response, postData){
        console.log("|__ Request handler 'test' Was called");
        
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("test");
        response.end();
 }

function ls(response, postData){
        console.log("|__ Request handler 'ls' Was called");

        exec("ls -lah", function(error, stdout, stderr){
                response.writeHead(200, {"Content-Type": "text/plain"});
                response.write(stdout);
                response.end();
        });
}

exports.start  = start;
exports.upload = upload;
exports.test = test;
exports.ls = ls;
